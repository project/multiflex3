<?php
?>
<div id="block-<?php print $block->module .'-'. $block->delta ?>" class="block block-<?php print $block->module ?> block-<?php print $block_id ?>">
  <?php if ($block->subject) : ?>
    <div class="corner-top-left"></div><div class="corner-top-right"></div>
    <h1><?php print $block->subject ?></h1>
  <?php endif; ?>
  <div class="content"><?php print $block->content ?></div>
</div>
